package br.com.client_rest_api.models.repositorys;

import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.client_rest_api.models.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {
	
	Optional<Client> findById(Long id);
	List<Client> findByCpfOrBirthDate(String cpf, Date date,Pageable pageable);
	List<Client> findAll(Pageable pageable);

}
