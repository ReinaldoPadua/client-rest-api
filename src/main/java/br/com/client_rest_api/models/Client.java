package br.com.client_rest_api.models;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.annotation.*;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.client_rest_api.enums.Status;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "clients")
public class Client {
	
	 @Id
     @GeneratedValue
     private Long id;
	 
	 @Column(name = "name")
	 private String name;
	 
	 @Column(name = "cpf", unique = true)
	 private String cpf;
	 
	 @JsonFormat(pattern="dd/MM/yyyy")
	 @DateTimeFormat(pattern = "dd/MM/yyyy")
	 @Temporal(TemporalType.TIMESTAMP)
	 @Column(name = "birth_date")
	 private Date birthDate;
	 	 
	 @Column(name = "status")
	 private Status status;
	 
	 private Long yearsOld;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getCpf() {
		return cpf.replaceAll("([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})","$1\\.$2\\.$3-$4"); 
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	@SuppressWarnings("deprecation")
	public Long getYearsOld() {
		return (long) (new Date().getYear() - this.birthDate.getYear());
	}

	public void setYearsOld(Long yearsOld) {
		this.yearsOld = yearsOld;
	}
	
}
