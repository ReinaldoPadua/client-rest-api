package br.com.client_rest_api.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.client_rest_api.dtos.ClientDto;
import br.com.client_rest_api.enums.Status;
import br.com.client_rest_api.models.Client;
import br.com.client_rest_api.models.repositorys.ClientRepository;

@Service
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	public Client newClient(ClientDto clientDto) throws ParseException {
		Client client = populateClientObject(new Client(),clientDto);
		client = clientRepository.save(client);
		return client;
	}
	
	public Client update(Long id, ClientDto clientDto) throws ParseException {
		Client client = populateClientObject(clientRepository.findById(id).get(),clientDto);
		client = clientRepository.save(client);
		return client;
	}
	
	private Client populateClientObject(Client client,ClientDto clientDto) throws ParseException {
		client.setName(clientDto.getName());
		client.setCpf(clientDto.getCpf());
		client.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse(clientDto.getBirthDate()));
		client.setStatus(Status.valueOf(clientDto.getStatus()));
		return client;
	}

}
