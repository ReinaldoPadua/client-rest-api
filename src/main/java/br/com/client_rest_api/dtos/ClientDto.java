package br.com.client_rest_api.dtos;


public class ClientDto {
	
	 private String name;
	 
	 private String cpf;
	 
	 private String birthDate;
	 
	 private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCpf() {
		return cpf.replaceAll("[^\\d.]", "");
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	
}
