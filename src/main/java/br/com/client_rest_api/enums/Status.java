package br.com.client_rest_api.enums;

public enum Status {
   New, Review, Approved, Rejected;
}
