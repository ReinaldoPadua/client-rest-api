package br.com.client_rest_api.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import br.com.client_rest_api.dtos.ClientDto;
import br.com.client_rest_api.enums.Status;
import br.com.client_rest_api.models.Client;
import br.com.client_rest_api.models.repositorys.ClientRepository;
import br.com.client_rest_api.services.ClientService;

@RestController
@RequestMapping("/clients")
public class ClientController {
	

	@Autowired
	private ClientService clientService;
	
	@Autowired
	private ClientRepository clientRepository;
	
	@GetMapping("/{id}")
	public Client getClient(@PathVariable(name="id")Long id) {
		return clientRepository.findById(id).orElse(null);
	}
	
	@GetMapping("/")
	public List<Client> listClients(@RequestParam(required = false) String cpf, 
			@RequestParam(required = false) String birthDate,
			 @RequestParam(defaultValue = "0") int page,
		     @RequestParam(defaultValue = "100") int size) throws ParseException {
		
		Pageable paging = PageRequest.of(page, size);
		
		if(cpf==null && birthDate==null ) 
			return clientRepository.findAll(paging);  
		return clientRepository.findByCpfOrBirthDate(cpf,new SimpleDateFormat("dd/MM/yyyy").parse(birthDate),paging );
	}
	
	@PostMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public void create(@RequestBody ClientDto clientDto) throws ParseException {
		clientService.newClient(clientDto);
	}
	
	@PutMapping("/{id}")
	public void update(@PathVariable Long id,@RequestBody ClientDto clientDto) throws ParseException {
		clientService.update(id,clientDto);
	}
	
	@PatchMapping("/{id}")
	public void updateWithPatch(@PathVariable Long id,@RequestBody ClientDto clientDto) throws ParseException {
		clientService.update(id,clientDto);
	}
	
	@DeleteMapping("/{id}")
	public void deleteClient(@PathVariable(name="id")Long id) {
		clientRepository.deleteById(id);;
	}
		
	@GetMapping("/status")
	public Status[] getStatus() {
		return Status.values();
	}
	
	
}
