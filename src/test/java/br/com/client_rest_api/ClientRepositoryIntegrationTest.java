package br.com.client_rest_api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;

import br.com.client_rest_api.dtos.ClientDto;
import br.com.client_rest_api.enums.Status;
import br.com.client_rest_api.models.Client;
import br.com.client_rest_api.models.repositorys.ClientRepository;
import br.com.client_rest_api.services.ClientService;


@DataJpaTest
class ClientRepositoryIntegrationTest {
	
	@Autowired
	private ClientRepository clientRepository;

	@Test
	void createUser() throws ParseException {
		
		Client client = initClient();
		
		Client clientSave = clientRepository.save(client);
		
		assertNotNull(clientSave.getId());
	     
	}
	
	@Test
	void listAllUsers() throws ParseException {
		
		Client client = initClient();
		clientRepository.save(client);
		
		Pageable paging = PageRequest.of(0, 100);
		List<Client> clients = clientRepository.findAll(paging);
		assertEquals(1, clients.size());
	     
	}
	
	@Test
	void listUsersByCpfAndDate() throws ParseException {
		
		Client client = initClient();
		clientRepository.save(client);
		
		Pageable paging = PageRequest.of(0, 100);
		List<Client> clients = clientRepository.findByCpfOrBirthDate("1112223339",
				new SimpleDateFormat("dd/MM/yyyy").parse("30/06/1986"), paging);
		assertEquals(1, clients.size());
	     
	}
	
	@Test
	void findById() throws ParseException {
		
		Client client = initClient();
		client = clientRepository.save(client);
		
		Client clientSaved = clientRepository.findById(client.getId()).get();
		assertEquals(client.getId(), clientSaved.getId());
	     
	}
	
	@Test
	void deleteUser() throws ParseException {
		
		Client client = initClient();
		client = clientRepository.save(client);
		clientRepository.deleteById(client.getId());

		Pageable paging = PageRequest.of(0, 100);
		List<Client> clients = clientRepository.findAll(paging);
		assertEquals(0, clients.size());
	     
	}
	
	
	private Client initClient() throws ParseException {
		Client client = new Client();
		client.setName("TESTE");
		client.setCpf("1112223339");
		client.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").parse("30/06/1986"));
		client.setStatus(Status.valueOf("New"));
		return client;
	}

}
